using System;
using System.Collections.Generic;
using System.Linq;

namespace MyServiceBus.Engine.Topics
{
    public class MyServiceBusTopicList
    {

        private Dictionary<string, MyServiceBusTopic> _topics = new ();

        public IReadOnlyList<MyServiceBusTopic> AllTopics { get; private set; } = Array.Empty<MyServiceBusTopic>();


        private readonly object _lockSection = new ();

        public MyServiceBusTopic AddIfNotExist(string topicId, long messageId)
        {
            
            lock (_lockSection)
            {
                if (_topics.ContainsKey(topicId))
                    return _topics[topicId];

                var topic = new MyServiceBusTopic(topicId, messageId);

                var newTopicList = new Dictionary<string, MyServiceBusTopic>(_topics)
                {
                    {topicId, topic}
                };
                _topics = newTopicList;

                AllTopics = _topics.Values.ToList();

                return topic;
            }
        }

        public MyServiceBusTopic TryGetTopic(string topicId)
        {
            var topics = _topics;

            return topics.TryGetValue(topicId, out var result) 
                ? result 
                : null;
        }


    }
}