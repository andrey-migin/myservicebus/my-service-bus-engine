using System;
using System.Collections.Generic;
using System.Linq;
using MyServiceBus.Engine.IndexedQueue;
using MyServiceBus.Engine.Messages;
using MyServiceBus.Engine.Queues;
using MyServiceBus.Persistence.Grpc;

namespace MyServiceBus.Engine.Topics
{
    public class MyServiceBusTopic
    {
        public string TopicId { get; }

        public long MessageId { get; private set; }

        private readonly object _topicMessageId = new ();
        
        private readonly object _topicQueues = new ();

        public TopicQueuesList Queues { get; }

        public MessagesPageList MessagesPageList { get; }
        
        public MyServiceBusTopic(string topicId, long messageId)
        {
            TopicId = topicId;
            MessageId = messageId;
            MessagesPageList = new MessagesPageList(topicId);
            Queues = new TopicQueuesList(this);
        }

        private IReadOnlyList<IMyServiceBusMessage> HandleMessages(IReadOnlyList<byte[]> messages, DateTime dateTime)
        {
            var result = new List<IMyServiceBusMessage>(messages.Count);
            lock (_topicMessageId)
            {
                foreach (var messageData in messages)
                {
                    var message = new MyServiceBusMessage(dateTime, MessageId, messageData);
                    MessageId++;
                    result.Add(message);
                }
                Queues.GetAccessorByTopic().Enqueue(result);
            }

            return result;
        }


        public IReadOnlyList<IMyServiceBusMessage> PublishMessages(IReadOnlyList<byte[]> messages, DateTime nowTime)
        {
            var resultMessages = HandleMessages(messages, nowTime);

            MessagesPageList.WriteLock(writer =>
            {
                writer.AddMessages(resultMessages);
            });

            return resultMessages;
        }

        public void Init(IEnumerable<QueueSnapshotGrpcModel> initDataQueues)
        {
            foreach (var initDataQueue in initDataQueues)
            {
                var indexRanges = initDataQueue.Ranges.Select(itm
                    => new QueueIndexRange(itm.FromId, itm.ToId));

                var queue = new TopicQueue(this, initDataQueue.QueueId, indexRanges,
                    false);
                Queues.GetAccessorByTopic().AddQueue(queue);
            }
        }


        public TopicQueue AddQueue(string queueId, bool deleteOnDisconnect)
        {
            lock (_topicQueues)
            {
                var queue = Queues.GetAccessorByTopic().TryGetQueue(queueId);


                if (queue != null)
                {
                    if (queue.DeleteOnDisconnect && !deleteOnDisconnect)
                        queue.MakeNonDeletable();
                    
                    return queue;
                }
                    
                
                queue = new TopicQueue(this, queueId, MessageId, deleteOnDisconnect);
                Queues.GetAccessorByTopic().AddQueue(queue);
                return queue;
            }
        }

        public TopicQueue TryGetQueue(string queueId)
        {
            lock (_topicQueues)
            {
                return Queues.GetAccessorByTopic().TryGetQueue(queueId);
            }
        }

        public void DeleteQueue(string queueId)
        {
            lock (_topicQueues)
            {
                Queues.GetAccessorByTopic().DeleteQueue(queueId);
            }
        }
    }
}