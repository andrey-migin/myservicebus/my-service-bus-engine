using System.Linq;
using MyServiceBus.Engine.IndexedQueue;
using MyServiceBus.Engine.Messages;
using MyServiceBus.Engine.Queues;
using MyServiceBus.Engine.Topics;
using MyServiceBus.Persistence.Grpc;

namespace MyServiceBus.Engine.BackgroundJobs
{
    public static class GrpcMappers
    {

        public static MessageContentGrpcModel ToGrpcModel(this IMyServiceBusMessage msg)
        {
            return new ()
            {
                MessageId = msg.Id,
                Created = msg.DateTime,
                Data = msg.Content
            };
        }

        public static MyServiceBusMessage ToDomain(this MessageContentGrpcModel grpcModel)
        {
            return new (grpcModel.Created, grpcModel.MessageId, grpcModel.Data);
        }


        public static TopicAndQueuesSnapshotGrpcModel ToTopicAndQueue(this MyServiceBusTopic topic)
        {
            return new ()
            {
                MessageId = topic.MessageId,
                TopicId = topic.TopicId,
                QueueSnapshots = topic.Queues
                    .ListOfQueues
                    .Where(itm => !itm.DeleteOnDisconnect)
                    .Select(itm => itm.ToGrpcModel())
                    .ToArray()
            };
        }

        public static QueueSnapshotGrpcModel ToGrpcModel(this TopicQueue topicQueue)
        {
            return new ()
            {
                QueueId = topicQueue.Id,
                Ranges = topicQueue.GetSnapshot().Select(itm => itm.ToGrpcModel()).ToArray()
            };
        }

        public static QueueIndexRangeGrpcModel ToGrpcModel(this IQueueIndexRange indexRange)
        {
            return new QueueIndexRangeGrpcModel
            {
                FromId = indexRange.FromId,
                ToId = indexRange.ToId
            };
        }
        
    }
}