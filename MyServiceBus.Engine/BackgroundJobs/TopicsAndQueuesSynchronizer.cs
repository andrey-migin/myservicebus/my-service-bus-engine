using System.Linq;
using System.Threading.Tasks;
using MyServiceBus.Engine.Topics;
using MyServiceBus.Persistence.Grpc;

namespace MyServiceBus.Engine.BackgroundJobs
{
    public class TopicsAndQueuesSynchronizer
    {
        private readonly MyServiceBusTopicList _topicList;
        private readonly IMyServiceBusQueuePersistenceGrpcService _persistenceGrpcService;

        public TopicsAndQueuesSynchronizer(MyServiceBusTopicList topicList, 
            IMyServiceBusQueuePersistenceGrpcService persistenceGrpcService)
        {
            _topicList = topicList;
            _persistenceGrpcService = persistenceGrpcService;
        }
        
        public ValueTask SyncAsync()
        {

            var topics = _topicList.AllTopics;
            
            var grpcRequest = new SaveQueueSnapshotGrpcRequest
            {
                QueueSnapshot = topics.Select(itm => itm.ToTopicAndQueue()).ToArray()
            };
            
            return _persistenceGrpcService.SaveSnapshotAsync(grpcRequest);
        }


        public async Task LoadTopicsAndQueuesAsync()
        {
            await foreach (var snapshot in _persistenceGrpcService.GetSnapshotAsync())
            {
                var topic = _topicList.AddIfNotExist(snapshot.TopicId, snapshot.MessageId);
                topic.Init(snapshot.QueueSnapshots);
            }
        }
    }
}