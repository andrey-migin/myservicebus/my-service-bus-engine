using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyServiceBus.Engine.Messages;
using MyServiceBus.Persistence.Grpc;

namespace MyServiceBus.Engine.BackgroundJobs
{
    public class MessagesPersistenceProcessor
    {
        private readonly MessagesPersistenceQueue _messagesPersistenceQueue;
        private readonly IMyServiceBusMessagesPersistenceGrpcService _myServiceBusMessagesPersistenceGrpcService;
        private readonly IMyServiceBusEngineSettings _myServiceBusEngineSettings;

        public MessagesPersistenceProcessor(MessagesPersistenceQueue messagesPersistenceQueue, 
            IMyServiceBusMessagesPersistenceGrpcService myServiceBusMessagesPersistenceGrpcService, 
            IMyServiceBusEngineSettings myServiceBusEngineSettings)
        {
            _messagesPersistenceQueue = messagesPersistenceQueue;
            _myServiceBusMessagesPersistenceGrpcService = myServiceBusMessagesPersistenceGrpcService;
            _myServiceBusEngineSettings = myServiceBusEngineSettings;
        }


        private async Task<bool> PersistMessagesAsync(Dictionary<string, SortedDictionary<long, IMyServiceBusMessage>> messagesToPersist)
        {

            var keys = messagesToPersist.Keys.ToList();

            foreach (var topicId in keys)
            {
                try
                {
                    var grpcMessages 
                        = messagesToPersist[topicId].Values.Select(itm => itm.ToGrpcModel());

                    await _myServiceBusMessagesPersistenceGrpcService.SaveMessagesAsync(topicId, grpcMessages.ToArray(), _myServiceBusEngineSettings.PersistencePacketSize);

                    messagesToPersist.Remove(topicId);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Can not persist messages of topic {topicId}");
                    Console.WriteLine(e);

                    foreach (var (topic, messages) in messagesToPersist)
                        _messagesPersistenceQueue.Enqueue(topic, messages.Values);
                }
            }

            return true;
        }

        public ValueTask<bool> ExecuteMessagesPersistenceAsync()
        {
            var messagesToPersist = _messagesPersistenceQueue.Dequeue();

            return messagesToPersist == null 
                ? new ValueTask<bool>(false) 
                : new ValueTask<bool>(PersistMessagesAsync(messagesToPersist));
        }
    }
}