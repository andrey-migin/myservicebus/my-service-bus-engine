using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyServiceBus.Engine.Messages;
using MyServiceBus.Engine.Topics;
using MyServiceBus.Persistence.Grpc;

namespace MyServiceBus.Engine.BackgroundJobs
{
    public class MessagePagesWarmerAndGc
    {
        private readonly MyServiceBusTopicList _topicList;
        private readonly IMyServiceBusMessagesPersistenceGrpcService _persistenceGrpcService;
        private readonly IMyServiceBusEngineSettings _settings;

        public MessagePagesWarmerAndGc(MyServiceBusTopicList topicList, 
            IMyServiceBusMessagesPersistenceGrpcService persistenceGrpcService, 
            IMyServiceBusEngineSettings settings)
        {
            _topicList = topicList;
            _persistenceGrpcService = persistenceGrpcService;
            _settings = settings;
        }


        private Dictionary<long, MessagePageId> GetActivePages(MyServiceBusTopic topic)
        {
            var result = new Dictionary<long, MessagePageId>();

            var lastMessagePageId = MessagesUtils.GetPageId(topic.MessageId);
            result.Add(lastMessagePageId.Value, lastMessagePageId);
            result.Add(lastMessagePageId.Value-1, new MessagePageId(lastMessagePageId.Value-1));

            foreach (var topicQueue in topic.Queues.ListOfQueues)
            {
                var topicQueueMessageId = MessagesUtils.GetPageId(topicQueue.GetMinMessageId());

                if (!result.ContainsKey(topicQueueMessageId.Value))
                    result.Add(topicQueueMessageId.Value, topicQueueMessageId);

                var prevQueuePageId = topicQueueMessageId.Value - 1;

                if (!result.ContainsKey(prevQueuePageId))
                    result.Add(prevQueuePageId, new MessagePageId(prevQueuePageId));

                var nextQueuePageId = topicQueueMessageId.Value + 1;

                if (nextQueuePageId < lastMessagePageId.Value)
                {
                    if (!result.ContainsKey(nextQueuePageId))
                        result.Add(nextQueuePageId, new MessagePageId(nextQueuePageId));
                }
            }

            return result;

        }


        private async Task WarmPagesAsync(Dictionary<string, (MyServiceBusTopic topic, List<MessagePageId> pages)> pagesToWarm)
        {
            foreach (var (topic, pageIds) in pagesToWarm.Values)
            {
                foreach (var pageId in pageIds)
                {
                    Console.WriteLine($"{DateTime.UtcNow:u} Warming up the page #{pageId.Value} for topic {topic.TopicId}");
                    var dt = DateTime.UtcNow;
                    var messages =
                        await _persistenceGrpcService
                            .GetPageAsync(topic.TopicId, pageId.Value)
                            .ToListAsync();

                    topic.MessagesPageList.WriteLock(writeAccess =>
                    {
                        writeAccess.AddMessages(messages.Select(msg => msg.ToDomain()));
                    });

                    var duration = DateTime.UtcNow - dt;
                    
                    Console.WriteLine($"{DateTime.UtcNow:u} Warmed up the page {pageId.Value} for topic {topic.TopicId} in {duration}");
                }
                
                topic.TryToDeliverNewMessages(_settings);
            }

        }

        private void GcPages(
            Dictionary<string, (MyServiceBusTopic topic, List<MessagePageId> pages)> pagesToGcDictionary)
        {
            foreach (var (topic, pagesToGc) in pagesToGcDictionary.Values)
            {
                    topic.MessagesPageList.WriteLock(writeAccess => writeAccess.RemovePages(pagesToGc));
            }
        }

        //ToDo - Create UnitTests
        public ValueTask WarmOrGcPagesAsync()
        {

            Dictionary<string, (MyServiceBusTopic topic, List<MessagePageId> pages)> pagesToWarm = null;

            Dictionary<string, (MyServiceBusTopic topic, List<MessagePageId> pages)> pagesToGc = null;

            foreach (var topic in _topicList.AllTopics)
            {

                var activePagesDesiredIds = GetActivePages(topic);
                
                // Checking pages to Warm
                foreach (var activePage in activePagesDesiredIds)
                {
                    var page = topic.MessagesPageList.ReadWithLock(readAccess => readAccess.TryGetPageForThe(activePage.Value));

                    if (page == null)
                    {
                        pagesToWarm ??= new Dictionary<string, (MyServiceBusTopic topic, List<MessagePageId> pages)>();
                        
                        if (!pagesToWarm.ContainsKey(topic.TopicId))
                            pagesToWarm.Add(topic.TopicId, (topic, new List<MessagePageId>()));
                        
                        pagesToWarm[topic.TopicId].pages.Add(activePage.Value);
                    }
                }
                
                // Checking pages to Gc

                var loadedPages = topic.MessagesPageList.AsList;
                foreach (var loadedPage in loadedPages)
                {

                    if (!activePagesDesiredIds.ContainsKey(loadedPage.PageId.Value))
                    {
                        pagesToGc ??= new Dictionary<string, (MyServiceBusTopic topic, List<MessagePageId> pages)>();
                        
                        if (!pagesToGc.ContainsKey(topic.TopicId))
                            pagesToGc.Add(topic.TopicId, (topic, new List<MessagePageId>()));
                        pagesToGc[topic.TopicId].pages.Add(loadedPage.PageId);
                    }
                    
                }
            }
            
            if (pagesToGc != null)
                GcPages(pagesToGc);

            return pagesToWarm == null
                ? new ValueTask() 
                : new ValueTask(WarmPagesAsync(pagesToWarm));
        }
        
        
    }
}