using System;
using System.Collections.Generic;
using System.Linq;

namespace MyServiceBus.Engine.IndexedQueue
{
    public class QueueWithIntervals
    {
        public QueueWithIntervals(IEnumerable<IQueueIndexRange> ranges)
        {
            foreach (var range in ranges)
                _intervals.Add(new QueueIndexRange(range.FromId, range.ToId));
        }

        public QueueWithIntervals(long messageId)
        {
            _intervals.Add(new QueueIndexRange(messageId));
        }

        private readonly List<QueueIndexRange> _intervals = new ();

        private int GetIndexToInsert(long messageId)
        {
            var i = 0;

            foreach (var range in _intervals)
            {
                if (range.IsBefore(messageId))
                    return i;

                i++;
            }

            return i;
        }

        private int GetIndexToDelete(long messageId)
        {

            var i = 0;

            foreach (var range in _intervals)
            {
                if (range.FromId <= messageId && messageId <= range.ToId)
                    return i;

                i++;
            }

            return -1;
        }

        private QueueIndexRange GetInterval(long messageId)
        {


            if (_intervals.Count == 1)
            {
                var firstOne = _intervals[0];
                if (firstOne.IsEmpty() || firstOne.IsMyInterval(messageId))
                    return firstOne;
            }

            foreach (var range in _intervals)
            {
                if (range.IsMyInterval(messageId))
                    return range;
            }

            var index = GetIndexToInsert(messageId);


            //ToDo - Weird that we enter here 0
            var newItem = new QueueIndexRange(0);

            if (index >= _intervals.Count)
                _intervals.Add(newItem);
            else
                _intervals.Insert(index, newItem);

            return newItem;

        }


        public IEnumerable<long> GetElements()
        {
            return _intervals.SelectMany(range => range.GetElements());
        }


        public void Enqueue(long messageId)
        {
            var interval = GetInterval(messageId);
            interval.AddNextMessage(messageId);

        }

        public long Dequeue()
        {
            var interval = _intervals[0];
            if (interval.IsEmpty())
                return -1;

            var result = interval.GetNextMessage();

            if (interval.IsEmpty() && _intervals.Count > 1)
                _intervals.RemoveAt(0);

            return result;

        }


        public override string ToString()
        {
            return $"Intervals: {_intervals.Count}. Count: {Count}";
        }

        public IReadOnlyList<QueueIndexRangeReadOnly> GetSnapshot()
        {
            return _intervals.Select(QueueIndexRangeReadOnly.Create).ToList();
        }


        public long GetMinId()
        {
            return _intervals[0].FromId;
        }

        public void Remove(in long messageId)
        {
            var index = GetIndexToDelete(messageId);

            if (index < 0)
            {
                Console.WriteLine("No element " + messageId);
                return;
            }

            var range = _intervals[index];

            if (range.FromId == messageId)
                range.FromId++;
            else if (range.ToId == messageId)
                range.ToId--;
            else
            {
                var newRange = QueueIndexRange.Create(range.FromId, messageId - 1);
                range.FromId = messageId + 1;
                _intervals.Insert(index, newRange);
            }

            if (range.IsEmpty() && _intervals.Count > 1)
                _intervals.RemoveAt(index);
        }

        public long Count => _intervals.Sum(itm => itm.Count());

    }
}