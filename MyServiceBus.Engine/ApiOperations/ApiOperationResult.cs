namespace MyServiceBus.Engine.ApiOperations
{
    public enum ApiOperationResult
    {
        Ok, TopicNotFound, QueueNotFound
    }
}