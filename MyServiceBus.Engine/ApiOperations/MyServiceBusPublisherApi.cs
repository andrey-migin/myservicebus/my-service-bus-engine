using System;
using System.Collections.Generic;
using MyServiceBus.Engine.Messages;
using MyServiceBus.Engine.Queues;
using MyServiceBus.Engine.Topics;

namespace MyServiceBus.Engine.ApiOperations
{
    
    public class MyServiceBusPublisherApi
    {
        private readonly MyServiceBusTopicList _topicList;
        private readonly MessagesPersistenceQueue _messagesPersistenceQueue;
        private readonly IMyServiceBusEngineSettings _settings;

        public MyServiceBusPublisherApi(MyServiceBusTopicList topicList, MessagesPersistenceQueue messagesPersistenceQueue, 
            IMyServiceBusEngineSettings settings)
        {
            _topicList = topicList;
            _messagesPersistenceQueue = messagesPersistenceQueue;
            _settings = settings;
        }

        public (ApiOperationResult result, IReadOnlyList<IMyServiceBusMessage> messages) Publish(string topicId, IReadOnlyList<byte[]> messagesToPublish, DateTime utcNow)
        {
            var topic = _topicList.TryGetTopic(topicId);
            
            if (topic == null)
                return (ApiOperationResult.TopicNotFound, null);

            var messages = topic.PublishMessages(messagesToPublish, utcNow);
            
            _messagesPersistenceQueue.Enqueue(topicId, messages);
            
            topic.TryToDeliverNewMessages(_settings);

            return (ApiOperationResult.Ok, messages);
        }


        public MyServiceBusTopic CreateTopicIfNotExists(string topicId)
        {
            return _topicList.AddIfNotExist(topicId, 0);
        }
        
    }
    
}