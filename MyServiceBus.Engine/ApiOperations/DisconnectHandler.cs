using System;
using System.Collections.Generic;
using MyServiceBus.Engine.Queues;

namespace MyServiceBus.Engine.ApiOperations
{
    public class DisconnectHandler
    {
        
        public void SubscribersAreDisconnected(IEnumerable<Subscriber> subscribers)
        {
            foreach (var subscriber in subscribers)
            {
                subscriber.Queue.UnSubscribe(subscriber);

                if (subscriber.Queue.SubscribersCount == 0 && subscriber.Queue.DeleteOnDisconnect)
                    subscriber.Queue.Topic.DeleteQueue(subscriber.Queue.Id);
            }
            
        }
    }
}