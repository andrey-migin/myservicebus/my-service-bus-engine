using System;
using System.Collections.Generic;
using MyServiceBus.Engine.IndexedQueue;
using MyServiceBus.Engine.Queues;
using MyServiceBus.Engine.Topics;

namespace MyServiceBus.Engine.ApiOperations
{
    public class MyServiceBusSubscriberApi
    {
        private readonly IMyServiceBusEngineSettings _settings;
        private readonly MyServiceBusTopicList _topicList;

        public MyServiceBusSubscriberApi(IMyServiceBusEngineSettings settings,
            MyServiceBusTopicList topicList)
        {
            _settings = settings;
            _topicList = topicList;
        }

        private (ApiOperationResult result, TopicQueue queue) GetQueue(string topicId, string queueId)
        {
            var topic = _topicList.TryGetTopic(topicId);
            if (topic == null)
                return (ApiOperationResult.TopicNotFound, null);


            var queue = topic.TryGetQueue(queueId);
            if (queue == null)
                return (ApiOperationResult.QueueNotFound, null);

            return (ApiOperationResult.Ok, queue) ;
        }

        public ApiOperationResult SetMessagesDeliveredOrNotDelivered(string topicId, string queueId, long deliveryId, bool delivered)
        {

            var (result, queue) = GetQueue(topicId, queueId);
            if (result != ApiOperationResult.Ok)
                return result;
            

            var subscriber = queue.GetSubscriber(deliveryId);

            if (subscriber == null)
                throw new Exception($"Subscriber with deliveryId: {deliveryId} and {topicId}/{queueId} is not found");
            
            if (queue.SetMessagesDeliveredOrNotDeliveredAndTryToDeliverNextChunk(subscriber, delivered, _settings.MaxDeliverySize))
                subscriber.DeliverMessages();

            return ApiOperationResult.Ok;
        }
        
        
        //ToDo - Unit Test It
        public ApiOperationResult SetSomeMessagesNotDelivered(string topicId, string queueId, long deliveryId, 
            IReadOnlyList<IQueueIndexRange> notDelivered)
        {

            var (result, queue) = GetQueue(topicId, queueId);
            if (result != ApiOperationResult.Ok)
                return result;
            

            var subscriber = queue.GetSubscriber(deliveryId);

            if (subscriber == null)
                throw new Exception($"Subscriber with deliveryId: {deliveryId} and {topicId}/{queueId} is not found");
            
            if (queue.SetMessagesDeliveredOrNotDeliveredAndTryToDeliverNextChunk(subscriber, notDelivered, _settings.MaxDeliverySize))
                subscriber.DeliverMessages();

            return ApiOperationResult.Ok;
        }

        public ApiOperationResult CreateQueue(string topicId, string queueId, bool deleteOnNoSubscribers)
        {
            var topic = _topicList.TryGetTopic(topicId);
            if (topic == null)
                return ApiOperationResult.TopicNotFound;

            topic.AddQueue(queueId, deleteOnNoSubscribers);

            return ApiOperationResult.Ok;
        }


        public (ApiOperationResult result, Subscriber subscriber) Subscribe(string topicId, string queueId,
            string connectionId)
        {
            var topic = _topicList.TryGetTopic(topicId);
            if (topic == null)
                return (ApiOperationResult.TopicNotFound, null);

            var queue = topic.TryGetQueue(queueId);

            if (queue == null)
                return (ApiOperationResult.QueueNotFound, null);

            var subscriber = queue.Subscribe(connectionId);
            return (ApiOperationResult.Ok, subscriber);
        }

    }
}