using System;
using System.Collections.Generic;
using System.Linq;
using MyDependencies;
using MyServiceBus.Engine.ApiOperations;
using MyServiceBus.Engine.IndexedQueue;
using MyServiceBus.Engine.Queues;
using MyServiceBus.Engine.Topics;

namespace MyServiceBus.Engine.Connections
{
    public class Connection
    {
        private readonly MyServiceBusPublisherApi _myServiceBusPublisherApi;
        private readonly MyServiceBusSubscriberApi _myServiceBusSubscriberApi;
        private readonly DisconnectHandler _disconnectHandler;


        private readonly List<Subscriber> _subscribers = new();
        
        public string ConnectionId { get; }

        public Connection(string connectionId, IServiceResolver sr)
        {
            ConnectionId = connectionId;
            _myServiceBusSubscriberApi = sr.GetService<MyServiceBusSubscriberApi>();
            _myServiceBusPublisherApi = sr.GetService<MyServiceBusPublisherApi>();
            _disconnectHandler = sr.GetService<DisconnectHandler>();
        }

        private readonly List<Action<Subscriber>> _messagesToDeliver
            = new();

        public void RegisterMessagesToDeliver(Action<Subscriber> messagesToDeliverCallback)
        {
            _messagesToDeliver.Add(messagesToDeliverCallback);
        }

        public MyServiceBusTopic CreateTopicIfNotExists(string topicId)
        {
            return _myServiceBusPublisherApi.CreateTopicIfNotExists(topicId);
        }


        public void Publish(string topicId, IReadOnlyList<byte[]> messagesToPublish, DateTime dt)
        {
            _myServiceBusPublisherApi.Publish(topicId, messagesToPublish, dt);
        }

        public (ApiOperationResult result, Subscriber subscriber) Subscribe(string topicId, string queueId, bool deleteOnDisconnect)
        {
            _myServiceBusSubscriberApi.CreateQueue(topicId, queueId, deleteOnDisconnect);
            
            var result = _myServiceBusSubscriberApi.Subscribe(topicId, queueId, ConnectionId);
            _subscribers.Add(result.subscriber);


            result.subscriber.SendMessagesCallback = subscriber =>
            {
                foreach (var callback in _messagesToDeliver)
                    callback(subscriber);
            };
            
            return result;
        }


        private Subscriber GetSubscriber(string topicId, string queueId)
        {
            var subscriber = _subscribers.FirstOrDefault(itm => itm.Queue.Id == queueId && itm.Queue.Topic.TopicId == topicId);

            if (subscriber == null)
                throw new Exception($"Can not find Subscriber to confirm message delivery. TopicId: {topicId}, ");

            return subscriber;
        }


        public ApiOperationResult ConfirmDelivery(string topicId, string queueId, long deliveryId,
            IReadOnlyList<IQueueIndexRange> notOk)
        {

            var subscriber = GetSubscriber(topicId, queueId);
            return _myServiceBusSubscriberApi.SetSomeMessagesNotDelivered(topicId, queueId, deliveryId, notOk);
        }

        public ApiOperationResult ConfirmDelivery(string topicId, string queueId, long deliveryId)
        {
            return _myServiceBusSubscriberApi.SetMessagesDeliveredOrNotDelivered(topicId, queueId, deliveryId, true);
        }

        public ApiOperationResult ConfirmNotDelivery(string topicId, string queueId, long deliveryId)
        {
            return _myServiceBusSubscriberApi.SetMessagesDeliveredOrNotDelivered(topicId, queueId, deliveryId, false);
        }

        public void Disconnect()
        {
            _messagesToDeliver.Clear();
            _disconnectHandler.SubscribersAreDisconnected(_subscribers);
        }
        
    }
}