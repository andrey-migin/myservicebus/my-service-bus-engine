namespace MyServiceBus.Engine
{
    public class AppGlobalFlags
    {
        public bool Initialized { get; set; }
        public bool Stopping { get; set; }
    }
}