namespace MyServiceBus.Engine
{
    
    public interface IMyServiceBusEngineSettings
    {
        int PersistencePacketSize { get; }
        int MaxDeliverySize { get; }
    }
}