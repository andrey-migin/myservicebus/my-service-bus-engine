using System;
using System.Collections.Generic;
using System.Linq;
using MyServiceBus.Engine.Messages;
using MyServiceBus.Engine.Topics;

namespace MyServiceBus.Engine.Queues
{

    public interface IAccessorByTopic
    {
        void Enqueue(IReadOnlyList<IMyServiceBusMessage> messages);

        void AddQueue(TopicQueue queue);

        void DeleteQueue(string queue);

        TopicQueue TryGetQueue(string queueId);

    }
    
    public class TopicQueuesList : IAccessorByTopic
    {
        private readonly Dictionary<string, TopicQueue> _queues = new ();

        public IReadOnlyList<TopicQueue> ListOfQueues { get; private set; } =  Array.Empty<TopicQueue>();

        public MyServiceBusTopic Topic { get; }

        public TopicQueuesList(MyServiceBusTopic topic)
        {
            Topic = topic;
        }

        public IAccessorByTopic GetAccessorByTopic()
        {
            return this;
        }

        void IAccessorByTopic.Enqueue(IReadOnlyList<IMyServiceBusMessage> messages)
        {
            foreach (var queue in ListOfQueues)    
                queue.Enqueue(messages);
        }

        void IAccessorByTopic.AddQueue(TopicQueue queue)
        {
            _queues.Add(queue.Id, queue);
            ListOfQueues = _queues.Values.ToList();
        }
        
        TopicQueue IAccessorByTopic.TryGetQueue(string queueId)
        {
            return _queues.TryGetValue(queueId, out var result) 
                ? result 
                : null;
        }

        public void DeleteQueue(string queueId)
        {
            _queues.Remove(queueId);
            ListOfQueues = _queues.Values.ToList();
        }


    }
}