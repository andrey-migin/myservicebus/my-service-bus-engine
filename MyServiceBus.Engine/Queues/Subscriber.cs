using System;
using System.Collections.Generic;
using MyServiceBus.Engine.Messages;

namespace MyServiceBus.Engine.Queues
{
    

    public class Subscriber
    {
        /// <summary>
        /// Id of Subscriber
        /// </summary>
        public string ConnectionId { get; }
        
        /// <summary>
        /// Id which identifies Subscriber which has data on delivery
        /// </summary>
        public long DeliveryId { get; }
        
        /// <summary>
        /// Queue the subscriber is subscribed
        /// </summary>
        public TopicQueue Queue { get; }
        
        public bool Rented { get; private set; }

        public Action<Subscriber> SendMessagesCallback { get; set; }
        
        public IReadOnlyList<IMyServiceBusMessage> MessagesOnDelivery { get; private set; }


        internal Subscriber(TopicQueue queue, string connectionId, long deliveryId)
        {
            Queue = queue;
            ConnectionId = connectionId;
            DeliveryId = deliveryId;
        }

        internal void DeliverMessages()
        {
            SendMessagesCallback((this));
        }

        internal void SetMessagesOnDelivery(IReadOnlyList<IMyServiceBusMessage> messages)
        {
            MessagesOnDelivery = messages;
        }

        internal bool TryToRentMe()
        {
            if (Rented)
                return false;

            Rented = true;
            return true;
        }

        internal void ReleaseRent()
        {

            if (!Rented)
                throw new Exception("Trying to release rent which is already released");

            MessagesOnDelivery = null;
            Rented = false;
        }

    }
}