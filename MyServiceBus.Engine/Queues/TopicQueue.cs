using System.Collections.Generic;
using MyServiceBus.Engine.IndexedQueue;
using MyServiceBus.Engine.Messages;
using MyServiceBus.Engine.Topics;

namespace MyServiceBus.Engine.Queues
{

    public class TopicQueueInitData
    {
        public string QueueId { get; }
        public bool DeleteOnDisconnect { get;  }
        public IEnumerable<IQueueIndexRange> IndexRanges { get; }
    }
    
    
    
    public partial class TopicQueue
    {
        public MyServiceBusTopic Topic { get; }
        public string Id { get; }

        private readonly QueueWithIntervals _queue;

        private readonly object _queueLockObject = new ();
        
        public bool DeleteOnDisconnect { get; private set; }


        public TopicQueue(MyServiceBusTopic topic, string queueId, 
            IEnumerable<IQueueIndexRange> ranges, bool deleteOnDisconnect)
        {
            Topic = topic;
            Id = queueId;
            _queue = new QueueWithIntervals(ranges);
            DeleteOnDisconnect = deleteOnDisconnect;
        }

        public TopicQueue(MyServiceBusTopic topic, string queueId, long messageId, bool deleteOnDisconnect)
        {
            Topic = topic;
            Id = queueId;
            _queue = new QueueWithIntervals(messageId);
            DeleteOnDisconnect = deleteOnDisconnect;
        }
        
        public void Enqueue(long messageId)
        {
            lock (_queueLockObject)
            {
                _queue.Enqueue(messageId);
            }
        }

        public void Enqueue(IEnumerable<IMyServiceBusMessage> messages)
        {
            lock (_queueLockObject)
            {
                foreach (var message in messages)
                    _queue.Enqueue(message.Id);    
            }
        }

        public long Dequeue()
        {
            lock (_queueLockObject)
            {
                return _queue.Dequeue();
            }
        }

        public IReadOnlyList<IQueueIndexRange> GetSnapshot()
        {
            lock (_queueLockObject)
            {
                return _queue.GetSnapshot();
            }
        }
        
        public long GetMinMessageId()
        {
            lock (_queueLockObject)
            {
                return _queue.GetMinId();
            }
        }

        public void MakeNonDeletable()
        {
            DeleteOnDisconnect = false;
        }


    }
}