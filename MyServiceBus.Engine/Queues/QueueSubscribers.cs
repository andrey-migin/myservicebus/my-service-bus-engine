using System;
using System.Collections.Generic;
using System.Linq;
using MyServiceBus.Engine.IndexedQueue;
using MyServiceBus.Engine.Messages;

namespace MyServiceBus.Engine.Queues
{
    public partial class TopicQueue
    {

        private readonly Dictionary<long, Subscriber> _subscribers = new ();
        private readonly Dictionary<string, Subscriber> _subscribersByConnectionId = new ();

        public IReadOnlyList<Subscriber> SubscribersAsList = Array.Empty<Subscriber>();
        
        
        public int SubscribersCount => SubscribersAsList.Count;

        private static long _deliveryId;

        
        internal Subscriber RentSubscriberToDeliverMessage(int maxDeliverySize)
        {
            lock (_queueLockObject)
            {
                foreach (var subscriber in SubscribersAsList)
                {
                    if (!subscriber.TryToRentMe()) 
                        continue;
                    
                    var messages = this.GetMessagesToDeliver(maxDeliverySize);

                    if (messages == null)
                    {
                        subscriber.ReleaseRent();
                        return null;
                    }

                    subscriber.SetMessagesOnDelivery(messages);
                    return subscriber;
                }

                return null;
            }
        }

        public Subscriber GetSubscriber(long deliveryId)
        {
            lock (_queueLockObject)
            {
                return _subscribers.TryGetValue(deliveryId, out var result) ? result : null;
            }
        }

        public Subscriber Subscribe(string connectionId)
        {
            lock (_queueLockObject)
            {
                if (_subscribersByConnectionId.ContainsKey(connectionId))
                    return _subscribersByConnectionId[connectionId];

                _deliveryId++;
                var result = new Subscriber(this, connectionId, _deliveryId);

                _subscribersByConnectionId.Add(result.ConnectionId, result);
                _subscribers.Add(result.DeliveryId, result);
                SubscribersAsList = _subscribers.Values.ToList();

                return result;
            }
        }
        
        public void UnSubscribe(Subscriber subscriber)
        {
            lock (_queueLockObject)
            {
                if (_subscribersByConnectionId.ContainsKey(subscriber.ConnectionId))
                {
                    _subscribersByConnectionId.Remove(subscriber.ConnectionId);
                    _subscribers.Remove(subscriber.DeliveryId);
                    SubscribersAsList = _subscribers.Values.ToList();
                }  
                

            }
            
            if (subscriber.Rented)
            {
                subscriber.Queue.Enqueue(subscriber.MessagesOnDelivery);
                subscriber.ReleaseRent();
            }
        }



        public bool SetMessagesDeliveredOrNotDeliveredAndTryToDeliverNextChunk(Subscriber subscriber, bool delivered, int maxMessagesToDeliver)
        {
            lock (_queueLockObject)
            {
                if (!delivered)
                  subscriber.Queue.Enqueue(subscriber.MessagesOnDelivery);

                var messages = this.GetMessagesToDeliver(maxMessagesToDeliver);
                
                if (messages == null)
                {
                    subscriber.ReleaseRent();
                    return false;
                }

                subscriber.SetMessagesOnDelivery(messages);
                return true;
            }
        }
        
        public bool SetMessagesDeliveredOrNotDeliveredAndTryToDeliverNextChunk(Subscriber subscriber, 
            IReadOnlyList<IQueueIndexRange> notDelivered, int maxMessagesToDeliver)
        {
            lock (_queueLockObject)
            {

                foreach (var msgId in notDelivered.GetElements())
                {
                    var message = subscriber.MessagesOnDelivery.FirstOrDefault(itm => itm.Id == msgId);
                    if (message == null)
                        Console.WriteLine($"Warning. Something is wrong. Can not find messaged {msgId} in delivery");
                    else
                        subscriber.Queue.Enqueue(message.Id);
                }
        

                var messages = this.GetMessagesToDeliver(maxMessagesToDeliver);
                
                if (messages == null)
                {
                    subscriber.ReleaseRent();
                    return false;
                }

                subscriber.SetMessagesOnDelivery(messages);
                return true;
            }
        }
        
        
 
        
        
    }
}