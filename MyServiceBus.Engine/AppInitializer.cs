using System;
using System.Threading.Tasks;
using MyServiceBus.Engine.BackgroundJobs;

namespace MyServiceBus.Engine
{
    public class AppInitializer
    {
        private readonly TopicsAndQueuesSynchronizer _topicsAndQueuesSynchronizer;
        private readonly AppGlobalFlags _appGlobalFlags;
        private readonly MessagesPersistenceProcessor _messagesPersistenceProcessor;

        public AppInitializer(TopicsAndQueuesSynchronizer topicsAndQueuesSynchronizer, 
            AppGlobalFlags appGlobalFlags,
            MessagesPersistenceProcessor messagesPersistenceProcessor)
        {
            _topicsAndQueuesSynchronizer = topicsAndQueuesSynchronizer;
            _appGlobalFlags = appGlobalFlags;
            _messagesPersistenceProcessor = messagesPersistenceProcessor;
        }

        public async Task InitializeAsync()
        {

            await _topicsAndQueuesSynchronizer.LoadTopicsAndQueuesAsync();
            _appGlobalFlags.Initialized = true;
        }


        public async Task StopAsync()
        {
            _appGlobalFlags.Stopping = true;

            Console.WriteLine("Delaying before start shutting down services");
            await Task.Delay(500);
            
            
            Console.WriteLine("Flushing messages persistence queue....");
            while (await _messagesPersistenceProcessor.ExecuteMessagesPersistenceAsync())
                await Task.Delay(100);
            
            Console.WriteLine("Synchronizing Queues snapshot....");

            await _topicsAndQueuesSynchronizer.SyncAsync();
            
            Console.WriteLine("Everything is stopped properly...");
        }
        
    }
}