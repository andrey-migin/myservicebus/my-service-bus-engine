using MyDependencies;
using MyServiceBus.Engine.ApiOperations;
using MyServiceBus.Engine.BackgroundJobs;
using MyServiceBus.Engine.Messages;
using MyServiceBus.Engine.Topics;

namespace MyServiceBus.Engine
{
    public static class MyServiceBusServicesBinder
    {

        public static void BindMyServiceBusServices(this IServiceRegistrator sr)
        {
            sr.Register<MyServiceBusTopicList>();
            
            sr.Register<DisconnectHandler>();
            sr.Register<MyServiceBusPublisherApi>();
            sr.Register<MyServiceBusSubscriberApi>();
            
            sr.Register<MessagesPersistenceProcessor>();
            
            sr.Register<MessagesPersistenceQueue>();
            sr.Register<MessagePagesWarmerAndGc>();
            sr.Register<AppGlobalFlags>();
            sr.Register<AppInitializer>();
            
        }
        
    }
}