namespace MyServiceBus.Engine.Messages
{

    public struct MessagePageId
    {
        public MessagePageId(long value)
        {
            Value = value;
        }
        
        public long Value { get; }
    }
    
    //ToDo - Put it to Library
    public static class MessagesUtils
    {

        public const int MessagesInPage = 100_000;

        public static MessagePageId GetPageId(long messageId)
        {
            var pageId = messageId / MessagesInPage;
            return new MessagePageId(pageId);
        }
        
        public static MessagePageId GetPageId(this IMyServiceBusMessage message)
        {
            return GetPageId(message.Id);
        }
        
    }
}