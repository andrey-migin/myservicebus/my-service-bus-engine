using System.Collections.Generic;

namespace MyServiceBus.Engine.Messages
{
    public class MessagesPersistenceQueue
    {

        private  Dictionary<string, SortedDictionary<long, IMyServiceBusMessage>> _queueToSave
            = new ();


        public void Enqueue(string topicId, IEnumerable<IMyServiceBusMessage> newMessages)
        {
            lock (_queueToSave)
            {
                if (!_queueToSave.ContainsKey(topicId))
                    _queueToSave.Add(topicId, new SortedDictionary<long, IMyServiceBusMessage>());

                var queueByTopic = _queueToSave[topicId];
                foreach (var msg in newMessages)
                {
                    if (!queueByTopic.ContainsKey(msg.Id))
                        queueByTopic.Add(msg.Id, msg);
                }
            }
        }

        public Dictionary<string, SortedDictionary<long, IMyServiceBusMessage>> Dequeue()
        {
            lock (_queueToSave)
            {

                if (_queueToSave.Count == 0)
                    return null;

                var result = _queueToSave;

                _queueToSave = new Dictionary<string, SortedDictionary<long, IMyServiceBusMessage>>();
                return result;
            }
        }
        
    }
}