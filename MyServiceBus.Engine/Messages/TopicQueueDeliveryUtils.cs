using System.Collections.Generic;
using MyServiceBus.Engine.Queues;
using MyServiceBus.Engine.Topics;

namespace MyServiceBus.Engine.Messages
{


    public static class TopicQueueDeliveryUtils
    {
        internal static IReadOnlyList<IMyServiceBusMessage> GetMessagesToDeliver(this TopicQueue topicQueue, int maxDeliverySize)
        {
            List<IMyServiceBusMessage> messages = null;

            var messageSize = 0;

            while (messageSize < maxDeliverySize)
            {
                var messageIdToDeliver = topicQueue.Dequeue();

                var pageId = MessagesUtils.GetPageId(messageIdToDeliver);

                var message = topicQueue.Topic.MessagesPageList.ReadWithLock(reader =>
                {
                    var messagesPage = reader.TryGetPageForThe(pageId);

                    if (messagesPage == null)
                    {
                        topicQueue.Enqueue(messageIdToDeliver);
                        return null;
                    }

                    var foundMessage= messagesPage.TryGetMessage(messageIdToDeliver);

                    if (foundMessage == null)
                    {
                        topicQueue.Enqueue(messageIdToDeliver);
                        return null;
                    }

                    return foundMessage;
                    
                });

                if (message == null)
                    break;

                messages ??= new List<IMyServiceBusMessage>();
                messages.Add(message);
                messageSize += message.Content.Length;
            }

            return messages;
        }



        internal static void TryToDeliverNewMessages(this MyServiceBusTopic topic, IMyServiceBusEngineSettings settings)
        {
            var queues = topic.Queues.ListOfQueues;

            foreach (var topicQueue in queues)
            {
                var subscriber = topicQueue.RentSubscriberToDeliverMessage(settings.MaxDeliverySize);
                while (subscriber != null)
                {
                    subscriber.DeliverMessages();
                    subscriber = topicQueue.RentSubscriberToDeliverMessage(settings.MaxDeliverySize);
                }

            }
        }


    }
}