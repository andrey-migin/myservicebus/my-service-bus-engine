using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace MyServiceBus.Engine.Messages
{

    
    public class MessagesPage
    {

        private readonly Dictionary<long, IMyServiceBusMessage>
            _messages = new ();
        
        public string TopicId { get; }
        public MessagePageId PageId { get; }

        public MessagesPage(string topicId, MessagePageId pageId)
        {
            TopicId = topicId;
            PageId = pageId;
        }

        public void AddOrUpdate(IEnumerable<IMyServiceBusMessage> messagesToUpdate)
        {
            foreach (var message in messagesToUpdate)
            {
                if (_messages.ContainsKey(message.Id))
                    _messages[message.Id] = message;
                else
                    _messages.Add(message.Id, message);
            }
        }

        public IMyServiceBusMessage TryGetMessage(long messageId)
        {
            return _messages.TryGetValue(messageId, out var result) 
                ? result 
                : null;
        }

    }

    public interface IMessagesWriteAccess
    {
        void AddMessages(IEnumerable<IMyServiceBusMessage> messages);
        
        void RemovePages(IEnumerable<MessagePageId> pageId);
    }


    public interface IMessagesReadAccess
    {

        MessagesPage TryGetPageForThe(MessagePageId pageId);

    }
    
    public class MessagesPageList : IMessagesWriteAccess, IMessagesReadAccess
    {

        private readonly Dictionary<long, MessagesPage> _pages = new ();

        public IReadOnlyList<MessagesPage> AsList { get; private set; } = Array.Empty<MessagesPage>();

        private readonly ReaderWriterLockSlim  _lockSlim = new ();
        public string TopicId { get; }
        
        public MessagesPageList(string topicId)
        {
            TopicId = topicId;
        }


        public T ReadWithLock<T>(Func<IMessagesReadAccess, T> readAccess)
        {
            _lockSlim.EnterReadLock();
            try
            {
               return readAccess(this);
            }
            finally
            {
                _lockSlim.ExitReadLock();
            }
        }

        public void WriteLock(Action<IMessagesWriteAccess> writeAccess)
        {
            _lockSlim.EnterWriteLock();
            try
            {
                writeAccess(this);
            }
            finally
            {
                _lockSlim.ExitWriteLock();
            }
        }


        void IMessagesWriteAccess.AddMessages(IEnumerable<IMyServiceBusMessage> messages)
        {
            var groupByPage = messages.GroupBy(itm => itm.GetPageId().Value);

            foreach (var pageMessages in groupByPage)
            {
                if (!_pages.ContainsKey(pageMessages.Key))
                {
                    _pages.Add(pageMessages.Key, new MessagesPage(TopicId, new MessagePageId(pageMessages.Key)));
                    AsList = _pages.Values.ToList();
                }
                
                _pages[pageMessages.Key].AddOrUpdate(pageMessages);
            }
            
        }

        void IMessagesWriteAccess.RemovePages(IEnumerable<MessagePageId> pagesId)
        {

            foreach (var pageId in pagesId)
            {
                if (!_pages.ContainsKey(pageId.Value))
                    return;

                _pages.Remove(pageId.Value);
            }
            AsList = _pages.Values.ToList();     
        }

        MessagesPage IMessagesReadAccess.TryGetPageForThe(MessagePageId pageId)
        {
            return _pages.TryGetValue(pageId.Value, out var result)
                ? result 
                : null;
        }
        
    }
    
}