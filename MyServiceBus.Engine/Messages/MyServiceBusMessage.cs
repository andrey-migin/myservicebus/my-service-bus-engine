using System;

namespace MyServiceBus.Engine.Messages
{

    public interface IMyServiceBusMessage
    {
        DateTime DateTime { get;  }
        long Id { get;  }
        
        public byte[] Content { get; }
    }
    
    public class MyServiceBusMessage : IMyServiceBusMessage
    {

        public MyServiceBusMessage(DateTime dateTime, long id, byte[] content)
        {
            DateTime = dateTime;
            Id = id;
            Content = content;
        }
        
        public DateTime DateTime { get;  }
        public long Id { get;  }
        public byte[] Content { get; }
    }
}