using System;
using MyServiceBus.Engine.ApiOperations;
using NUnit.Framework;

namespace MyServiceBus.Engine.Tests
{
    public class Tests
    {

        /// <summary>
        /// We create one topic, one subscriber
        /// We publish one message
        /// We assert that message is on Delivery
        /// We do not confirm delivery
        /// </summary>
        [Test]
        public void TestMessageDeliveryToOneOfTheSubscriber()
        {
            var ioc = TestUtils.GetTestIoc();

            var connectionPublisher = ioc.Connect();

            var topicId = "testTopic";
            connectionPublisher.Connection.CreateTopicIfNotExists(topicId);

            const string queueName = "testQueue";
            
            var (_, subscriber) = connectionPublisher.Connection.Subscribe(topicId, queueName, true);

            var message = new byte[] {1, 2, 3};

            var time = DateTime.UtcNow;

            var result = ioc.PublishMessage(topicId, message, time);
            
            Assert.AreEqual(ApiOperationResult.Ok, result.result);
            Assert.AreEqual(1, subscriber.MessagesOnDelivery.Count);
            Assert.AreEqual(1, connectionPublisher.Messages[queueName].Count);

            var lastDelivery = connectionPublisher.GetLastDelivery(queueName);
            
            lastDelivery.messages[0].Content.EqualsWith(message);
            
        }


       
        /// <summary>
        /// We create topic and Queue
        /// We publish two messages 3 bytes size each
        /// We make limit on delivery size as 3 bytes which enables sending the second package only after we confirm the first one
        /// Assertion:
        /// We deliver the second message after we confirm delivery of the first one
        /// </summary>

        [Test]
        public void TestTestTwoMessagesDeliveryWithConfirmation()
        {
            var ioc = TestUtils.GetTestIoc();
            
            var connectionPublisher = ioc.Connect();

            var topicId = "testTopic";
            connectionPublisher.Connection.CreateTopicIfNotExists(topicId);
            
            var connectionSubscriber = ioc.Connect();
            const string queueName = "testQueue";
            connectionSubscriber.Connection.Subscribe(topicId, queueName, true);

            var msg1 = new byte[] {1, 2, 3};
            var msg2 = new byte[] {4};
            var msg3 = new byte[] {5,6};
            var dt = DateTime.UtcNow;
            // First message Publish
            connectionPublisher.Connection.Publish(topicId, new []{msg1, msg2, msg3}, dt);

            var lastDelivery = connectionSubscriber.GetLastDelivery(queueName);
            
            Assert.AreEqual(1, lastDelivery.messages.Count);
            
            connectionSubscriber.Connection.ConfirmDelivery(topicId, queueName, lastDelivery.deliveryId);
            
            lastDelivery = connectionSubscriber.GetLastDelivery(queueName);
            Assert.AreEqual(2, lastDelivery.messages.Count);

        }
        
        
        
        
    }
}