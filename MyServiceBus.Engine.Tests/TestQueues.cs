using NUnit.Framework;

namespace MyServiceBus.Engine.Tests
{
    public class TestQueues
    {


        /// <summary>
        /// If we create AutoDelete Queue - and we disconnect - the quest must be deleted
        /// </summary>
        [Test]
        public void TestQueueAutoDeleted()
        {
            var ioc = TestUtils.GetTestIoc();
            
            var connectionPublisher = ioc.Connect();

            const string topicName = "test";
            var topic = connectionPublisher.Connection.CreateTopicIfNotExists(topicName);

            var connectionSubscriber = ioc.Connect();
            const string queueName = "test";

            connectionSubscriber.Connection.Subscribe(topicName, queueName, true);
            
            Assert.AreEqual(1, topic.Queues.ListOfQueues.Count);
            
            connectionSubscriber.Connection.Disconnect();
            Assert.AreEqual(0, topic.Queues.ListOfQueues.Count);
        }
        
        /// <summary>
        /// If we create AutoDelete Queue - and we disconnect - the quest must be alive
        /// </summary>
        [Test]
        public void TestQueueNotAutoDeleted()
        {
            var ioc = TestUtils.GetTestIoc();
            
            var connectionPublisher = ioc.Connect();

            const string topicName = "test";
            var topic = connectionPublisher.Connection.CreateTopicIfNotExists(topicName);

            var connectionSubscriber = ioc.Connect();
            const string queueName = "test";

            connectionSubscriber.Connection.Subscribe(topicName, queueName, false);
            
            Assert.AreEqual(1, topic.Queues.ListOfQueues.Count);
            
            connectionSubscriber.Connection.Disconnect();
            Assert.AreEqual(1, topic.Queues.ListOfQueues.Count);
        }
        
    }
}