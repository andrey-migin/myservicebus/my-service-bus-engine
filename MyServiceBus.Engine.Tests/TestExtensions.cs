using System.Collections.Generic;
using System.Linq;
using MyServiceBus.Engine.Messages;
using NUnit.Framework;

namespace MyServiceBus.Engine.Tests
{
    public static class TestExtensions
    {

        public static void EqualsWith(this byte[] src, byte[] dest)
        {
            Assert.AreEqual(src.Length, dest.Length);

            for (var i = 0; i < src.Length; i++)
            {
                Assert.AreEqual(src[i], dest[i]);
            }
        }

        public static void EqualsWith(this IMyServiceBusMessage src, IMyServiceBusMessage dest)
        {
            Assert.AreEqual(src.Id, dest.Id);
            Assert.AreEqual(src.DateTime, dest.DateTime);
            src.Content.ToArray().EqualsWith(dest.Content.ToArray());
        }

        public static void EqualsWith(this IReadOnlyList<IMyServiceBusMessage> src,
            IReadOnlyList<IMyServiceBusMessage> dest)
        {
            Assert.AreEqual(src.Count, dest.Count);


            foreach (var srcMessage in src)
            {
                var destMessage = dest.First(itm => itm.Id == srcMessage.Id);
                srcMessage.EqualsWith(destMessage);
            }
        }
        
    }
}