using System;
using MyDependencies;
using MyServiceBus.Engine.ApiOperations;
using MyServiceBus.Engine.Messages;
using MyServiceBus.Engine.Tests.Mocks;
using MyServiceBus.Persistence.Grpc;

namespace MyServiceBus.Engine.Tests
{
    public static class TestUtils
    {

        public static MyIoc GetTestIoc()
        {
            var ioc = new MyIoc();

            ioc.BindMyServiceBusServices();
            
            ioc.Register<IMyServiceBusEngineSettings>(new MessagesDeliverySettingsMock());
            ioc.Register<IMyServiceBusMessagesPersistenceGrpcService>(new MyServiceBusMessagesPersistenceGrpcServiceMock());

            return ioc;
        }

        public static MyIoc WithMaxDeliveryMessageSize(this MyIoc myIoc, int deliverySize)
        {
            var settings = (MessagesDeliverySettingsMock)myIoc.GetService<IMyServiceBusEngineSettings>();

            settings.MaxDeliverySize = deliverySize;
            return myIoc;
        }


        public static (ApiOperationResult result, IMyServiceBusMessage message) PublishMessage(this MyIoc ioc, string topicId, byte[] message, DateTime? utcNow = null)
        {
            utcNow ??= DateTime.UtcNow;
            var (result, messages) = ioc.GetService<MyServiceBusPublisherApi>().Publish(topicId, new[]{message}, utcNow.Value);
            return (result, messages[0]);
        }


        public static TestConnectionMock Connect(this MyIoc ioc)
        {
            return new (ioc);
        }
    }
}