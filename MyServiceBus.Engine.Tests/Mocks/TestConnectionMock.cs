using System;
using System.Collections.Generic;
using System.Linq;
using MyDependencies;
using MyServiceBus.Engine.Connections;
using MyServiceBus.Engine.Messages;
using MyServiceBus.Engine.Queues;

namespace MyServiceBus.Engine.Tests.Mocks
{
    public class TestConnectionMock
    {

        public readonly Dictionary<string, List<(long delivertId, IReadOnlyList<IMyServiceBusMessage> messages)>> Messages =
            new ();


        public TestConnectionMock(IServiceResolver sr)
        {
            Connection = new Connection(Guid.NewGuid().ToString(), sr);
            Connection.RegisterMessagesToDeliver(MessageToDeliver);
        }

        
        public Connection Connection { get; }

        
        public void MessageToDeliver(Subscriber subscriber)
        {
            if (!Messages.ContainsKey(subscriber.Queue.Id))
                Messages.Add(subscriber.Queue.Id, new List<(long delivertId, IReadOnlyList<IMyServiceBusMessage> messages)>());
            
            Messages[subscriber.Queue.Id].Add((subscriber.DeliveryId, subscriber.MessagesOnDelivery));
        }


        public (long deliveryId, IReadOnlyList<IMyServiceBusMessage> messages) GetLastDelivery(string queueId)
        {
            return Messages[queueId].Last();
        }
        
    }
    
}