using System.Collections.Generic;
using System.Threading.Tasks;
using MyServiceBus.Persistence.Grpc;

namespace MyServiceBus.Engine.Tests.Mocks
{
    public class MyServiceBusMessagesPersistenceGrpcServiceMock : IMyServiceBusMessagesPersistenceGrpcService
    {

        private Dictionary<string, Dictionary<long, List<byte[]>>> _data = new();

        private object _lockObject = new();
        
        
        public async IAsyncEnumerable<byte[]> GetPageCompressedAsync(GetMessagesPageGrpcRequest request)
        {
            lock (_lockObject)
            {
                if (!_data.ContainsKey(request.TopicId)) 
                    yield break;
                
                var dictByTopic = _data[request.TopicId];

                if (!dictByTopic.ContainsKey(request.PageNo)) 
                    yield break;
                    
                foreach (var bytes in dictByTopic[request.PageNo])
                {
                    yield return bytes;
                }
            }
        }

        public ValueTask SaveMessagesAsync(IAsyncEnumerable<byte[]> request)
        {
            throw new System.NotImplementedException();
        }

        public ValueTask<MessageContentGrpcModel> GetMessageAsync(GetMessageGrpcRequest request)
        {
            throw new System.NotImplementedException();
        }
    }
}