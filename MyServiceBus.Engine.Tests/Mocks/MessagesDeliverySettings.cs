using MyServiceBus.Engine.BackgroundJobs;

namespace MyServiceBus.Engine.Tests.Mocks
{
    public class MessagesDeliverySettingsMock : IMyServiceBusEngineSettings
    {
        public int PersistencePacketSize { get; set; } = 1024*1024;
        public int MaxDeliverySize { get; set; } = 3;
    }
}