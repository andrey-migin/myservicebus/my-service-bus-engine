Synchronization Sections
  * topics list level - to synchronize add/remove sections;
  * topic level -  _topicMessageId - to synchronize assigning messageId to each message
  * topic queues - Queues - to synchronize threads on add/remove queues
  * topic messages level - _queueLockObject to add/remove messages to the queue
